#ifndef SQUARECOUNTER_H
#define SQUARECOUNTER_H

#include <QtWidgets/QMainWindow>
#include "ui_squarecounter.h"

class SquareCounter : public QMainWindow
{
	Q_OBJECT

public:
	SquareCounter(QWidget *parent = 0);
	~SquareCounter();

private:
	Ui::squareCounterClass ui;
};

#endif // SQUARECOUNTER_H
