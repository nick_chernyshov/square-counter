#include "curvePainter.h"
#include <QPainter>
#include <QtMath>

#define _USE_MATH_DEFINES

using namespace std;

CurvePainter::CurvePainter(QWidget *parent)
	: QWidget(parent), m_radius(50), m_origin(m_radius, m_radius)
{
	ui.setupUi(this);


	array<double, 4> angles;
	array<QPointF, 4> points;

	for (int i = 0; i < 24; i+=3)
	{
		for (int j = 0; j < 4; j++)
		{
			double val = 2.0 * M_PI * (i + j) / 24.0;
			angles[j] = val;
		}

		for (int j = 0; j < 4; j++)
		{
			points[j] = m_origin + QPointF(cos(angles[j]), sin(angles[j])) * m_radius;
			m_angles.push_back(points[j].x());
			m_angles.push_back(points[j].y());
		}
		m_curves.push_back(Bezier3(points));

	}

/*	m_curves.push_back(Bezier3({ QPointF(0.0, 0.0), QPointF(15.0, 0.0), QPointF(30.0, 0.0), QPointF(45.0, 0.0) }, 0));
	m_curves.push_back(Bezier3({ QPointF(45.0, 0.0), QPointF(30.0, 15.0), QPointF(15.0, 30.0), QPointF(0.0, 45.0) }, 0.0));
	m_curves.push_back(Bezier3({ QPointF(0.0, 45.0), QPointF(0.0, 30.0), QPointF(0.0, 15.0), QPointF(0.0, 0.0) }, 0.0));*/

	for (auto & it : m_curves)
	{
		auto & points = it.m_points;
		path.moveTo(points[0]);
		path.cubicTo(points[1], points[2], points[3]);
	}
	path.closeSubpath();
}

CurvePainter::~CurvePainter()
{

}

void CurvePainter::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	//! [8] //! [9]
	QLinearGradient gradient(0, 0, 0, 100);
	gradient.setColorAt(0.0, qRgb(32, 32, 32));
	gradient.setColorAt(1.0, qRgb(64, 64, 64));
//	painter.setBrush(gradient);
	painter.drawPath(path);
}
