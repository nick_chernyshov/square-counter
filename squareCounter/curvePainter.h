#ifndef CURVEPAINTER_H
#define CURVEPAINTER_H

#include <QWidget>
#include "Bezier3.h"

#include "ui_curvePainter.h"

class CurvePainter : public QWidget
{
	Q_OBJECT

public:
	CurvePainter(QWidget *parent = 0);
	~CurvePainter();

	QVector<double> m_angles;
	double square()
	{
		double sum = 0;
		for (auto & it : m_curves)
			sum += it.integrate();
		return sum;
	}
	void CurvePainter::paintEvent(QPaintEvent *);
private:
	Ui::curvePainter ui;
	QPainterPath path;	
	double m_radius;
	QPointF m_origin;
	QVector<Bezier3> m_curves;
};

#endif // CURVEPAINTER_H
