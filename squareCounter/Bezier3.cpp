#include "Bezier3.h"

// binomialCoefficient(n , k)

Bezier3::Bezier3(const std::array<QPointF, 4> & points)
{
	m_points = points;
}

QPointF Bezier3::f(double t)
{
	auto & p0 = m_points[0];
	auto & p1 = m_points[1];
	auto & p2 = m_points[2];
	auto & p3 = m_points[3];
	return p0 + 3 * p1* t + 3 * p2* t*t + (-p0 - 3 * p1 - 3 * p2 + p3) * t*t*t;
}
QPointF Bezier3::df(double t)
{
	auto & p0 = m_points[0];
	auto & p1 = m_points[1];
	auto & p2 = m_points[2];
	auto & p3 = m_points[3];
	return 3 * p1 + 6 * p2 * t + 3 * (p3 - p0 - 3 * p1 - 3 * p2 + p3) * t * t;
}

double Bezier3::integrate()
{
	auto & p0 = m_points[0];
	auto & p1 = m_points[1];
	auto & p2 = m_points[2];
	auto & p3 = m_points[3];

	double x0 = p0.x();
	double x1 = p1.x();
	double x2 = p2.x();
	double x3 = p3.x();

	double y0 = p0.y();
	double y1 = p1.y();
	double y2 = p2.y();
	double y3 = p3.y();

	return -((3.0 * x1*y0) / 10) - (3.0 * x2*y0) / 20 - (x3*y0) / 20 + (3.0 * x0*y1) / 10 - (3.0 * x2*y1) / 20 -
		(3.0 * x3*y1) / 20 + (3.0 * x0*y2) / 20 + (3.0 * x1*y2) / 20 - (3.0 * x3*y2) / 10 + (x0*y3) / 20 +
		(3.0 * x1*y3) / 20 + (3.0 * x2*y3) / 10;
}

