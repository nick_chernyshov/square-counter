#include "squarecounter.h"

SquareCounter::SquareCounter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	QString sOut;
	for (auto & it : ui.curvePainter->m_angles)
	{
		sOut += QString("%1\n").arg(it);
	}
	sOut += QString("Square = %1\n").arg(ui.curvePainter->square());
	ui.textEdit->setText(sOut);
}

SquareCounter::~SquareCounter()
{

}
