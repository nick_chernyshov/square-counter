#ifndef CURVE_H
#define CURVE_H
#include <QWidget>
#include <map>
#include <array>

class Bezier3
{
public:
	Bezier3() : m_points({ QPointF(0.0, 0.0), QPointF(0.0, 1.0), QPointF(1.0, 1.0), QPointF(1.0, 0.0) }) {};
	Bezier3(const std::array<QPointF, 4> & points);
	~Bezier3() {}
public:
	std::array<QPointF, 4> m_points;
public:
	QPointF f(double t); // value at parameter t
	QPointF df(double t); // derivative at parameter t
	double integrate(); // curvature integral over the length

};

#endif // CURVE_H
