/********************************************************************************
** Form generated from reading UI file 'squarecounter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SQUARECOUNTER_H
#define UI_SQUARECOUNTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include "curvepainter.h"

QT_BEGIN_NAMESPACE

class Ui_squareCounterClass
{
public:
    QWidget *centralWidget;
    CurvePainter *curvePainter;
    QTextEdit *textEdit;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *squareCounterClass)
    {
        if (squareCounterClass->objectName().isEmpty())
            squareCounterClass->setObjectName(QStringLiteral("squareCounterClass"));
        squareCounterClass->resize(600, 400);
        centralWidget = new QWidget(squareCounterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        curvePainter = new CurvePainter(centralWidget);
        curvePainter->setObjectName(QStringLiteral("curvePainter"));
        curvePainter->setGeometry(QRect(10, 10, 211, 321));
        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(230, 10, 161, 321));
        squareCounterClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(squareCounterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 21));
        squareCounterClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(squareCounterClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        squareCounterClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(squareCounterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        squareCounterClass->setStatusBar(statusBar);

        retranslateUi(squareCounterClass);

        QMetaObject::connectSlotsByName(squareCounterClass);
    } // setupUi

    void retranslateUi(QMainWindow *squareCounterClass)
    {
        squareCounterClass->setWindowTitle(QApplication::translate("squareCounterClass", "squareCounter", 0));
    } // retranslateUi

};

namespace Ui {
    class squareCounterClass: public Ui_squareCounterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SQUARECOUNTER_H
