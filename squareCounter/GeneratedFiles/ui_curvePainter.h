/********************************************************************************
** Form generated from reading UI file 'curvePainter.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CURVEPAINTER_H
#define UI_CURVEPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_curvePainter
{
public:

    void setupUi(QWidget *curvePainter)
    {
        if (curvePainter->objectName().isEmpty())
            curvePainter->setObjectName(QStringLiteral("curvePainter"));
        curvePainter->resize(400, 300);

        retranslateUi(curvePainter);

        QMetaObject::connectSlotsByName(curvePainter);
    } // setupUi

    void retranslateUi(QWidget *curvePainter)
    {
        curvePainter->setWindowTitle(QApplication::translate("curvePainter", "curvePainter", 0));
    } // retranslateUi

};

namespace Ui {
    class curvePainter: public Ui_curvePainter {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CURVEPAINTER_H
